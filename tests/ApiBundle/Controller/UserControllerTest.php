<?php

namespace ApiBundle\Tests\Controller;

use Symfony\Component\HttpFoundation\Response;

class UserControllerTest extends BaseWebTest
{
    protected $fixturesClassNames = ['AppBundle\DataFixtures\ORM\LoadUserData'];

    public function testGetUsers()
    {
        $response = $this->doRequestByUri('/api/users', 'GET', ['offset' => 20, 'limit' => 100]);

        $this->assertJsonResponse($response);
        $this->assertResponseStatus($response, Response::HTTP_OK);

        $content = json_decode($response->getContent(), true);

        $this->assertArrayHasKey('users', $content);
        $this->assertArrayHasKey('_metadata', $content);
        $this->assertArrayHasKey('limit', $content['_metadata']);
        $this->assertArrayHasKey('offset', $content['_metadata']);
        $this->assertArrayHasKey('totalCount', $content['_metadata']);

        $this->assertNotEmpty($content['users']);

        $this->assertEquals(100, $content['_metadata']['limit']);
        $this->assertEquals(20, $content['_metadata']['offset']);
        $this->assertEquals(100, count($content['users']));
    }

    public function testGetUser()
    {
        $users = json_decode($this->doRequestByUri('/api/users')->getContent(), true)['users'];
        $id = $users[array_rand($users, 1)]['id'];

        $response = $this->doRequestByUri('/api/users/' . $id);

        $this->assertJsonResponse($response);
        $this->assertResponseStatus($response, Response::HTTP_OK);

        $content = json_decode($response->getContent(), true);

        $this->assertNotEmpty($content['id']);
        $this->assertArrayHasKey('firstName', $content);
        $this->assertArrayHasKey('lastName', $content);
    }

    public function testUserNoFound()
    {
        $id = 'not_existing';
        $response = $this->doRequestByUri('/api/users/' . $id);

        $this->assertJsonResponse($response);
        $this->assertResponseStatus($response, Response::HTTP_NOT_FOUND);
    }

    public function testGetUserConnections()
    {
        $users = json_decode($this->doRequestByUri('/api/users')->getContent(), true)['users'];
        $id = $users[array_rand($users, 1)]['id'];

        $response = $this->doRequestByUri('/api/users/' . $id . '/connections');

        $this->assertJsonResponse($response);
        $this->assertResponseStatus($response, Response::HTTP_OK);

        $content = json_decode($response->getContent(), true);

        $this->assertArrayHasKey('connections', $content);
    }
}
