<?php

namespace ApiBundle\Tests\Controller;

use Liip\FunctionalTestBundle\Test\WebTestCase;
use Symfony\Component\HttpFoundation\Response;

abstract class BaseWebTest extends WebTestCase
{
    /**
     * @var array
     */
    protected $fixturesClassNames = [];

    /**
     * @var \Symfony\Bundle\FrameworkBundle\Client
     */
    protected $client;

    /**
     * @var \Doctrine\Common\DataFixtures\ReferenceRepository
     */
    protected $fixtures;

    /**
     * Load Fixtures
     */
    protected function fixturesLoad()
    {
        $this->fixtures = $this->loadFixtures($this->fixturesClassNames)->getReferenceRepository();
    }

    /**
     * Setups the environment, Test DB and Fixtures for the Tests
     */
    protected function setUp()
    {
        $this->client = static::createClient([], ['HTTP_HOST' => 'naumov.test']);
        $this->fixturesLoad();
    }

    /**
     * Assert Json Response and its status code
     *
     * @param Response $response The Response object from the Request
     */
    protected function assertJsonResponse(Response $response)
    {
        $this->assertTrue(
            $response->headers->contains('Content-Type', 'application/json'),
            $response->headers
        );
    }

    /**
     * @param \Symfony\Component\HttpFoundation\Response $response
     * @param integer $expectedStatus
     */
    protected function assertResponseStatus(Response $response, $expectedStatus)
    {
        $this->assertEquals($expectedStatus, $response->getStatusCode());
    }

    /**
     * @param $uri
     * @param string $method
     * @param array $parameters
     * @return \Symfony\Component\HttpFoundation\Response
     */
    protected function doRequestByUri($uri, $method = 'GET', $parameters = [])
    {
        $this->client->request($method, $uri, $parameters);

        return $this->client->getResponse();
    }
}
