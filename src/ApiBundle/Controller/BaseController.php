<?php

namespace ApiBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class BaseController extends Controller
{
    /**
     * @param array $data
     * @param array $metaData
     * @param int $code
     * @return JsonResponse
     */
    public function renderExtendedResponse(array $data, $metaData, $code = Response::HTTP_OK)
    {
        $data = array_merge($data, ['_metadata' => $metaData]);

        return $this->renderResponse($data, $code);
    }

    /**
     * @param int $status
     * @return JsonResponse
     */
    public function renderEmptyResponse($status = Response::HTTP_NO_CONTENT)
    {
        return $this->renderResponse(null, $status);
    }

    /**
     * @param $data
     * @param int $code
     * @return JsonResponse
     */
    public function renderResponse($data, $code = Response::HTTP_OK)
    {
        return new JsonResponse($data, $code);
    }

    /**
     * @param Request $request
     * @return array
     */
    protected function getParams(Request $request)
    {
        $params = $request->request->count() ? $request->request->all() : json_decode($request->getContent(), true);

        return empty($params) ? [] : $params;
    }
}
