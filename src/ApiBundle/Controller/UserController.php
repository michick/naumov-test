<?php

namespace ApiBundle\Controller;

use AppBundle\Exception\UserNotFoundException;
use Doctrine\DBAL\Exception\UniqueConstraintViolationException;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class UserController extends BaseController
{
    /**
     * @Route("/users/{id}", name="get_user")
     * @Method({"GET"})
     */
    public function getUserAction($id)
    {
        $userService = $this->get('app.user_service');

        try {
            $user = $userService->get($id);
        } catch (UserNotFoundException $e) {
            return $this->renderResponse([
                'message' => $e->getMessage(),
            ], Response::HTTP_NOT_FOUND);
        }

        return $this->renderResponse($user->toArray());
    }

    /**
     * @Route("/users", name="get_users")
     * @Method({"GET"})
     */
    public function getUsersAction(Request $request)
    {
        $userService = $this->get('app.user_service');
        $params = $request->query->all();
        $criteria = $userService->getCriteria($params);

        $users = $userService->getAllByCriteria($criteria);
        //@todo proper serialization
        $usersData = [];
        foreach ($users as $user) {
            $usersData[] = $user->toArray();
        }

        return $this->renderExtendedResponse(['users' => $usersData], [
            'limit' => $criteria->getMaxResults(),
            'offset' => $criteria->getFirstResult(),
            'totalCount' => $userService->count($criteria),
        ]);
    }

    /**
     * @Route("/users/{id}/connections", name="get_user_connections")
     * @Method({"GET"})
     */
    public function getUserConnectionsAction($id)
    {
        $userService = $this->get('app.user_service');

        try {
            $connections = $userService->getConnections($id);
        } catch (UserNotFoundException $e) {
            return $this->renderResponse([
                'message' => $e->getMessage(),
            ], Response::HTTP_NOT_FOUND);
        }

        return $this->renderResponse([
            'connections' => $connections,
        ]);
    }

    /**
     * @Route("/users/{id}/connections/{connectionId}", name="create_user_connection")
     * @Method({"GET"}) @todo should be POST
     */
    public function createUserConnectionAction($id, $connectionId)
    {
        $userService = $this->get('app.user_service');

        try {
            $userService->createConnection($id, $connectionId);
        } catch (UserNotFoundException $e) {
            return $this->renderResponse([
                'message' => $e->getMessage(),
            ], Response::HTTP_NOT_FOUND);
        } catch (UniqueConstraintViolationException $e) {
            return $this->renderResponse([
                'message' => 'Connection already exists.',
            ], Response::HTTP_BAD_REQUEST);
        }

        return $this->renderResponse([
            'message' => 'Created',
        ], Response::HTTP_CREATED);
    }
}
