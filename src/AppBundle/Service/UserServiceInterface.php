<?php

namespace AppBundle\Service;

use Doctrine\Common\Collections\Criteria;

interface UserServiceInterface
{
    /**
     * @param string $id
     * @return \AppBundle\Entity\User
     * @throws \AppBundle\Exception\UserNotFoundException
     */
    public function get($id);

    /**
     * @param \Doctrine\Common\Collections\Criteria $criteria
     * @return \AppBundle\Entity\User[]
     */
    public function getAllByCriteria(Criteria $criteria);

    /**
     * @param array $params
     * @return \Doctrine\Common\Collections\Criteria
     */
    public function getCriteria(array $params);

    /**
     * @param Criteria $criteria
     * @return int
     */
    public function count(Criteria $criteria);

    /**
     * @param string $userId
     * @return string[]
     * @throws \AppBundle\Exception\UserNotFoundException
     */
    public function getConnections($userId);

    /**
     * @param $id1
     * @param $id2
     * @throws \AppBundle\Exception\UserNotFoundException
     */
    public function createConnection($id1, $id2);
}