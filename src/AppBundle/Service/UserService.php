<?php

namespace AppBundle\Service;

use AppBundle\Exception\UserNotFoundException;
use AppBundle\Repository\UserRepositoryInterface;
use AppBundle\Repository\UserToUserRepositoryInterface;
use Doctrine\Common\Collections\Criteria;

class UserService implements UserServiceInterface
{
    /**
     * @var \AppBundle\Repository\UserRepositoryInterface
     */
    protected $repo;

    /**
     * @var \AppBundle\Repository\UserToUserRepositoryInterface
     */
    protected $userToUserRepo;

    public function __construct(UserRepositoryInterface $repo, UserToUserRepositoryInterface $userToUserRepo)
    {
        $this->repo = $repo;
        $this->userToUserRepo = $userToUserRepo;
    }

    /**
     * {@inheritdoc}
     */
    public function get($id)
    {
        $user = $this->repo->find($id);

        if (empty($user)) {
            throw new UserNotFoundException('User with id: ' . $id . ' not found.');
        }

        return $user;
    }

    /**
     * {@inheritdoc}
     */
    public function getAllByCriteria(Criteria $criteria)
    {
        return $this->repo->findAllWithCriteria($criteria);
    }

    /**
     * @param array $params
     * @return Criteria
     */
    public function getCriteria(array $params)
    {
        $params = $this->prepareParams($params);
        $criteria = new Criteria();

        foreach ($params as $name => $value) {
            switch ($name) {
                case 'limit':
                    $criteria->setMaxResults($value);
                    break;
                case 'offset':
                    $criteria->setFirstResult($value);
                    break;
                case 'orderBy':
                    $fields = is_array($value) ? $value : [$value];
                    $fields = array_fill_keys($fields, 'ASC');

                    $criteria->orderBy($fields);
                    break;
                default:
                    $criteria->andWhere(Criteria::expr()->eq($name, $value));
            }
        }

        return $criteria;
    }

    /**
     * @param array $params
     * @return array
     */
    protected function prepareParams(array $params)
    {
        $params = array_filter($params, function($param) {
            return $param !== null;
        });

        $params['limit'] = !empty($params['limit']) ? intval($params['limit']) : 50;
        $params['offset'] = !empty($params['offset']) ? intval($params['offset']) : 0;
        $params['orderBy'] = !empty($params['orderBy']) ? $params['orderBy'] : [];

        return $params;
    }

    /**
     * {@inheritdoc}
     */
    public function count(Criteria $criteria)
    {
        return $this->repo->count($criteria);
    }

    /**
     * {@inheritdoc}
     */
    public function getConnections($userId)
    {
        $this->get($userId);

        return $this->userToUserRepo->getConnections($userId);
    }

    /**
     * {@inheritdoc}
     */
    public function createConnection($id1, $id2)
    {
        $user1 = $this->get($id1);
        $user2 = $this->get($id2);

        $this->userToUserRepo->saveConnection($user1->getId(), $user2->getId());
    }
}