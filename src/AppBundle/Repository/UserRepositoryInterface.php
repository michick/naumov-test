<?php

namespace AppBundle\Repository;

use Doctrine\Common\Collections\Criteria;

interface UserRepositoryInterface
{
    /**
     * @param string $id
     * @return \AppBundle\Entity\User
     */
    public function find($id);

    /**
     * @param Criteria $criteria
     * @return \AppBundle\Entity\User[]
     */
    public function findAllWithCriteria(Criteria $criteria);

    /**
     * @param Criteria $criteria
     * @return int
     */
    public function count(Criteria $criteria);
}