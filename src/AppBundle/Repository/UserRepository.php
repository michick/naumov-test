<?php

namespace AppBundle\Repository;

use Doctrine\Common\Collections\Criteria;

class UserRepository extends \Doctrine\ORM\EntityRepository implements UserRepositoryInterface
{
    /**
     * {@inheritdoc}
     */
    public function findAllWithCriteria(Criteria $criteria)
    {
        return $this->matching($criteria)->getValues();
    }

    /**
     * {@inheritdoc}
     */
    public function count(Criteria $criteria)
    {
        $countCriteria = clone $criteria;
        $countCriteria->setMaxResults(null)->setFirstResult(null);

        $count = $this->createQueryBuilder('o')
            ->select('COUNT(o)')
            ->addCriteria($countCriteria)
            ->getQuery()
            ->getSingleScalarResult();

        return intval($count);
    }
}
