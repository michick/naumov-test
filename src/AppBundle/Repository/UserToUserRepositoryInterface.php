<?php

namespace AppBundle\Repository;

interface UserToUserRepositoryInterface
{
    /**
     * @param string $userId
     * @return string[]
     */
    public function getConnections($userId);

    /**
     * @param string $id1
     * @param string $id2
     */
    public function saveConnection($id1, $id2);

    /**
     * @param string $id1
     * @param string $id2
     * @return bool
     */
    public function connectionExists($id1, $id2);
}