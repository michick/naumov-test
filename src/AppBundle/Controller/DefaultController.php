<?php

namespace AppBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class DefaultController extends Controller
{
    /**
     * @Route("/", name="homepage")
     */
    public function indexAction()
    {
        $userService = $this->get('app.user_service');
        $criteria = $userService->getCriteria(['limit' => 100]);
        $users = $userService->getAllByCriteria($criteria);

        $user1 = $users[array_rand($users, 1)];
        $user2 = $users[array_rand($users, 1)];

        return $this->render('@App/Default/index.html.twig', [
            'user1' => $user1,
            'user2' => $user2,
        ]);
    }
}
