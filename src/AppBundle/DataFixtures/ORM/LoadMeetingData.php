<?php

namespace AppBundle\DataFixtures\ORM;

use AppBundle\Entity\Meeting;
use Doctrine\Common\DataFixtures\FixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use Faker\Factory;

class LoadMeetingData implements FixtureInterface
{
    /**
     * {@inheritdoc}
     */
    public function load(ObjectManager $manager)
    {
        $faker = Factory::create();

        for ($i = 0; $i < 150; $i++) {
            $meeting = new Meeting();
            $meeting->setName($faker->company);
            $meeting->setLocation($faker->address);
            $meeting->setTime($faker->dateTime);

            $manager->persist($meeting);
        }

        $manager->flush();
    }
}