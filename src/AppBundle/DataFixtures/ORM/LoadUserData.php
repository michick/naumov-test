<?php

namespace AppBundle\DataFixtures\ORM;

use AppBundle\Entity\User;
use AppBundle\Entity\UserToUser;
use Doctrine\Common\DataFixtures\FixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use Faker\Factory;

class LoadUserData implements FixtureInterface
{
    /**
     * {@inheritdoc}
     */
    public function load(ObjectManager $manager)
    {
        $faker = Factory::create();
        $ids = [];

        for ($i = 0; $i < 200; $i++) {
            $user = new User();
            $user->setFirstName($faker->firstName);
            $user->setLastName($faker->lastName);

            $manager->persist($user);
            $ids[] = $user->getId();
        }

        $manager->flush();

        $repo = $manager->getRepository('AppBundle\Entity\UserToUser');

        foreach ($ids as $id) {
            $connections = array_rand($ids, random_int(2, 10));

            foreach ($connections as $connection) {
                if ($repo->connectionExists($id, $ids[$connection])) {
                    continue;
                }

                $userToUser = new UserToUser();
                $userToUser->setId1($id);
                $userToUser->setId2($ids[$connection]);

                $manager->persist($userToUser);
            }

            $manager->flush();
        }
    }
}