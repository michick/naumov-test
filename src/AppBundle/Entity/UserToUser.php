<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * User2User
 *
 * @ORM\Table(name="user_to_user", indexes={@ORM\Index(name="idxid1", columns={"id1"})})
 * @ORM\Entity(repositoryClass="AppBundle\Repository\UserToUserRepository")
 *
 * @ORM\HasLifecycleCallbacks()
 */
class UserToUser
{
    /**
     * @var string
     *
     * @ORM\Column(name="id1", type="string", length=36)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="NONE")
     */
    private $id1;

    /**
     * @var string
     *
     * @ORM\Column(name="id2", type="string", length=36)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="NONE")
     */
    private $id2;


    /**
     * Set id1
     *
     * @param string $id1
     *
     * @return UserToUser
     */
    public function setId1($id1)
    {
        $this->id1 = $id1;

        return $this;
    }

    /**
     * Get id1
     *
     * @return string
     */
    public function getId1()
    {
        return $this->id1;
    }

    /**
     * Set id2
     *
     * @param string $id2
     *
     * @return UserToUser
     */
    public function setId2($id2)
    {
        $this->id2 = $id2;

        return $this;
    }

    /**
     * Get id2
     *
     * @return string
     */
    public function getId2()
    {
        return $this->id2;
    }

    /**
     * @ORM\PrePersist
     */
    public function orderIds()
    {
        if ($this->getId1() > $this->getId2()) {
            $tmp = $this->getId1();
            $this->setId1($this->getId2());
            $this->setId2($tmp);
        }
    }
}
