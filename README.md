naumov-test
===========

Tested on:
----------
* host machine OS: Ubuntu 16.04
* Virtual Box 5.1.8
* Vagrant 1.8.6
* Ansible

Requirements:
-------------
* Git
* VirtualBox
* Vagrant
* Ansible

Set up:
-------
```
git clone {path}
cd {project_folder}
vagrant up
```
Vagrant will install missing plugins.
Create vm.
Also will ask password for editing hosts file: add naumov.test.

Done:
-----
Go to http://naumov.test/app_dev.php

Tests:
------
```
vagrant ssh
cd /var/www/naumov-test
phpunit
```